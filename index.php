<?php

use Singularity\SingularityDB;

include 'vendor/autoload.php';

$sing = new SingularityDB();


$sing->db->distinct()->select('id_client, name, lastName, age, country');
$sing->db->from('clients');
$sing->db->where('id_client', 1, '>');
$sing->db->whereIn('id_client', [1, 2, 3, 4, 5], true);
$sing->db->limit(3);
$sing->db->prepareSelect();
echo $sing->db->showSql();
$clients = $sing->db->runSelectList();
var_dump($clients);


