<?php namespace Singularity;


use Singularity\Drivers\SingularDBE;

/**
 * undocumented class
 */
class SingularityDB {


    private static array $mdsLoaded = [];

    protected string $currentMds = '';
    
    public SingularDBE $db;

    public function __construct() {
        $this->db = $this->setDb(DatabaseConfig::$defaultCnx);
    }


    public function setDb(string $cnx) : SingularDBE {
        $dbConf = new DatabaseConfig();
        $this->currentMds = $cnx;
        if ((SingularityDB::$mdsLoaded[$cnx] ?? null) === null) {
            $driver = 'Singularity\\Drivers\\'.$dbConf->connections[$cnx]['driver'];
            SingularityDB::$mdsLoaded[$cnx] = new $driver($dbConf->connections[$cnx]);
        }
        return SingularityDB::$mdsLoaded[$cnx];
    }

    

}
