<?php namespace Singularity;

/**
 * DatabaseConfig
 * 
 * Configuración de las conexiones a las bases de datos que se desea acceder
 * Mediante la propiedad $connections se podra establecer la configuración ya sea en
 * un ambiente local o de producción para conect arse a una o varias bases de datos a la vez.
 * 
 * Debe tener en cuenta que el nombre de los drivers a usar debe ser exacto, caso contrario podria
 * mostrarse un error en la ejecución
 */
class DatabaseConfig {

    /**
     * Modificando la propiedad $defaultCnx, podemos establecer cual sera la conexion que se usara
     * por defecto
     */
    public static string $defaultCnx = 'default';

    public array $connections = [
        'default' => [
            'host' => 'localhost',
            'user' => 'root',
            'password' => '',
            'database' => 'helluva',
            'driver' => 'MySql',
            'encKey' => '', // encription Hash
        ],
        'test' => [
            'host' => 'localhost',
            'user' => 'root',
            'password' => '***',
            'database' => 'anotherDB',
            'driver' => 'MySql',
            'encKey' => '', // encription Hash
        ]
    ];
}
