<?php namespace Singularity\Drivers;

/**
 * Driver Mysql
 * 
 * Driver de Singularity destinado especialmente a conexiones con bases de datos de MySql
 * Clase concreta de la abstraccion SingularDBE
 */
class MySql extends SingularDBE {

    public function connect() : void {
        $this->cnx = mysqli_connect(
            $this->dbConfig['host'],
            $this->dbConfig['user'],
            $this->dbConfig['password'],
            $this->dbConfig['database']
        );
    }

    public final function closeConnection() : void { $this->cnx->close(); }

    public final function prepareSelect(string $sql = '') : self {
        if ($sql !== '') {
            $this->sql = $sql;
            return $this;
        }
        $this->sql = 'SELECT '.$this->distinct.$this->select." \n";
        $this->sql .= 'FROM '.$this->from." \n";
        foreach ($this->join as $j) {
            $this->sql .= $j['side'].' JOIN '.$j['table'].' ON '.$j['conditional']." \n";
        }

        if ($this->where !== []) {
            $this->sql .= 'WHERE '.$this->buildWhere()." \n";
        }
        if ($this->groupBy !== []) { $this->sql .= 'GROUP BY '.implode(', ', $this->groupBy)." \n"; }
        if ($this->orderBy !== []) { $this->sql .= 'ORDER BY '.implode(', ', $this->orderBy)." \n"; }
        if ($this->limit !== '') { $this->sql .= $this->limit; }
        return $this;
    }

    public final function runSelectList(string $type = 'object') : array {
        $r = [];
        $stm = $this->cnx->query($this->sql);
        $this->clearVars();
        if ($type == 'object') { while ($res = $stm->fetch_object()) { $r[] = $res; } }
        if ($type == 'array') { while ($res = $stm->fetch_assoc()) { $r[] = $res; } }
        return $r;
    }

    public final function fetchSelect(string $type = 'object') : ?object {
        $stm = $this->cnx->query($this->sql);
        $this->clearVars();
        if ($type = 'object') { return $stm->fetch_object(); }
        $queryResult = $stm->fetch_assoc();
        if (($queryResult ?? null) === null) { return null; }
        $obj = new $type;
        foreach ($queryResult as $k => $val) { $obj->$k = $val; }
        return $obj;
    }

    public final function limit(string $limit, ?string $offset = null) : self {
        $this->limit = 'LIMIT ' . $limit . ($offset === null ? '' : (', '.$offset));
        return $this;
    }

    public final function esc(string $str) : string {
        return $this->cnx->real_escape_string($str);
    }

    public final function callSP(string $spName, ...$arguments) : self {
        $this->sql = 'CALL '.$spName.'('."'".implode("', '", ...$arguments)."'".')';
        return $this;
    }
}
