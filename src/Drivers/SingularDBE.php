<?php namespace Singularity\Drivers;

/**
 * undocumented class
 */
abstract class SingularDBE {
    
    protected array $dbConfig;
    protected string $sql = '';
    protected string $distinct = '';
    protected string $select = '';
    protected string $from = '';
    protected array $join = [];
    protected array $where = [];
    protected array $orderBy = [];
    protected array $groupBy = [];
    protected string $limit = '';


    private string $groupStart = '';

    public function __construct(array $config) {
        $this->dbConfig = $config;
        $this->connect();
    }

    public function __destruct() { $this->cnx->close(); }

    /**
     * Funcion closeConnection
     *
     * Destruye la conexión actualmente establecida, puede usarse para liberar la memoria durante la ejecución de el programa
     *
     * @return vois
     **/
    public abstract function closeConnection() : void ;

    /**
     * Funcion connect
     *
     * Establece la conexion con la base de datos determinada por la configuración determinada por DatabaseConfig.php
     *
     * @return vois
     **/
    public abstract function connect() : void;

    /**
     * Funcion prepareSelect
     *
     * Prepara los datos almacenados en memoria para armar una instrucción select que sera ejecutada mediante runSelectList o fetchSelect
     *
     * @param String $sql En caso de que sea necesario, se puede usar una consulta personalizada la cual reemplazara a la almacenada en memoria, esto seria util si es que se desea hacer instrucciones select con UNION
     * @return SingularDBE
     **/
    public abstract function prepareSelect(string $sql = '') : self;

    /**
     * Funcion runSelectList
     *
     * Ejecuta una instrucción "Select" previamente armada y retorna las filas resultantes
     *
     * @param String $type Determina el tipo de dato que contendra la matriz resultante, stdClass por defecto, se puede jugar con los tipos de datos a retornar para obtener listas dinamicas 
     * @return Array
     **/
    public abstract function runSelectList(string $type = 'object') : array;

    /**
     * Funcion limit
     *
     * Establece un limite en la sentencia select a preparar mediante el traductor de Singularity
     *
     * @param String $limit primer argumento de la instrucción Limit 
     * @param ?String $offset segundo argumento de la instrucción Limit 
     * @return SingularDBE
     **/
    public abstract function limit(string $limit, ?string $offset = null) : self;


    /**
     * Funcion esc
     *
     * Escapa las entradas previas a ser enviadas como argumento a una consulta
     *
     * @param String $str Cadena a proteger en la ejecución de la instrucción SQL
     * @return String
     **/
    public abstract function esc(string $str) : string;

    /**
     * Funcion call
     *
     * Este metodo esta destinado a invocar y preparar Procedimientos Almacenados
     *
     * @param String $spName Nombre del procedimiento almacenado
     * @return SingularDBE
     **/
    public abstract function callSP(string $spName, ...$arguments) : self ;
    
    

    public function select(string $select = '*') : SingularDBE {
        $this->select = $select;
        return $this;
    }

    public function from(string $from) : SingularDBE {
        $this->from = $from;
        return $this;
    }

    public function join(string $table, string $conditional, string $side = 'INNER') : SingularDBE {
        $this->join[] = [
            'table' => $table,
            'conditional' => $conditional,
            'side' => $side,
        ];
        return $this;
    }


    protected function clearVars() : void {
        $this->sql = '';
        $this->distinct = '';
        $this->select = '';
        $this->from = '';
        $this->join = [];
        $this->where = [];
        $this->orderBy = [];
        $this->groupBy = [];
        $this->limit = '';
        $this->groupStart = '';
    }


    protected function buildWhere() : string {
        $where = '';
        foreach ($this->where as $w) {
            $where .= ($w['prevCondition'] !== '' ? (''.$w['prevCondition'].'') : '').(' '.$w['field']).' '.$w['operator'].' '.$w['value'].' ';
        }
        return trim($where);
    }


    public function whereIn(string $field, array $data, bool $numeric = false) : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('AND'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => $numeric === false ? '('."'".implode("', '", $data)."'".')' : '('.implode(', ', $data).')',
            'operator' => 'IN',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function whereNotIn(string $field, array $data, bool $numeric = false) : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('AND'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => $numeric === false ? '('."'".implode("', '", $data)."'".')' : '('.implode(', ', $data).')',
            'operator' => 'NOT IN',
        ];
        $this->groupStart = '';
        return $this;
    }
    
    public function orWhereIn(string $field, array $data, bool $numeric = false) : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('OR'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => $numeric === false ? '('."'".implode("', '", $data)."'".')' : '('.implode(', ', $data).')',
            'operator' => 'IN',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function orWhereNotIn(string $field, array $data, bool $numeric = false) : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('OR'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => $numeric === false ? '('."'".implode("', '", $data)."'".')' : '('.implode(', ', $data).')',
            'operator' => 'NOT IN',
        ];
        $this->groupStart = '';
        return $this;
    }

    
    public function between(string $field, string $masterVal, string $slaveVal) : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('AND'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => is_numeric($field) ? $field : ("'".$field."'"),
            'value' => (is_numeric($masterVal) ? $masterVal : ("'".$masterVal."'")).' AND '.(is_numeric($slaveVal) ? $slaveVal : ("'".$slaveVal."'")),
            'operator' => 'BETWEEN',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function orBetween(string $field, string $masterVal, string $slaveVal) : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('OR'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => is_numeric($field) ? $field : ("'".$field."'"),
            'value' => (is_numeric($masterVal) ? $masterVal : ("'".$masterVal."'")).' AND '.(is_numeric($slaveVal) ? $slaveVal : ("'".$slaveVal."'")),
            'operator' => 'BETWEEN',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function Notbetween(string $field, string $masterVal, string $slaveVal) : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('AND'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => is_numeric($field) ? $field : ("'".$field."'"),
            'value' => (is_numeric($masterVal) ? $masterVal : ("'".$masterVal."'")).' AND '.(is_numeric($slaveVal) ? $slaveVal : ("'".$slaveVal."'")),
            'operator' => 'NOT BETWEEN',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function orNotbetween(string $field, string $masterVal, string $slaveVal) : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('OR'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => is_numeric($field) ? $field : ("'".$field."'"),
            'value' => (is_numeric($masterVal) ? $masterVal : ("'".$masterVal."'")).' AND '.(is_numeric($slaveVal) ? $slaveVal : ("'".$slaveVal."'")),
            'operator' => 'NOT BETWEEN',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function where(string $field, ?string $value,  string $operator = '=') : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('AND'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => ($value ?? null) === null ? 'NULL' : $value,
            'operator' => $operator,
        ];
        $this->groupStart = '';
        return $this;
    }

    public function orWhere(string $field, ?string $value,  string $operator = '=') : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('OR'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => ($value ?? null) === null ? 'NULL' : $value,
            'operator' => $operator,
        ];
        $this->groupStart = '';
        return $this;
    }

    public function like(string $field, ?string $value,  string $side = 'LR') : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('AND'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => ($side === 'LR' || $side == 'L' ? '%' : '').($value ?? '').($side === 'LR' || $side == 'R' ? '%' : ''),
            'operator' => 'LIKE',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function notLike(string $field, ?string $value,  string $side = 'LR') : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('AND'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => ($side === 'LR' || $side == 'L' ? '%' : '').($value ?? '').($side === 'LR' || $side == 'R' ? '%' : ''),
            'operator' => 'NOT LIKE',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function orLike(string $field, ?string $value,  string $side = 'LR') : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('OR'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => ($side === 'LR' || $side == 'L' ? '%' : '').($value ?? '').($side === 'LR' || $side == 'R' ? '%' : ''),
            'operator' => 'LIKE',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function orNotLike(string $field, ?string $value,  string $side = 'LR') : self {
        $this->where[] = [
            'prevCondition' => $this->where === [] ? ($this->groupStart) : ('OR'.($this->groupStart !== '' ? (' '.$this->groupStart) : '')),
            'field' => $field,
            'value' => ($side === 'LR' || $side == 'L' ? '%' : '').($value ?? '').($side === 'LR' || $side == 'R' ? '%' : ''),
            'operator' => 'NOT LIKE',
        ];
        $this->groupStart = '';
        return $this;
    }

    public function groupStart() : self {
        $this->groupStart .= '(';
        return $this;
    }

    public function groupEnd() : self {
        $lastIndex = array_key_last($this->where);
        $this->where[$lastIndex]['value'] = $this->where[$lastIndex]['value'].')';
        return $this;
    }

   



    public function distinct() : self {
        $this->distinct = 'DISTINCT ';
        return $this;
    }

    public function orderBy(string $field, string $orderMode = 'ASC') : self {
        $this->orderBy[] = $field.' '.$orderMode;
        return $this;
    }

    public function groupBy(string $field) : self {
        $this->groupBy[] = $field;
        return $this;
    }

    public function showSql(bool $show = false) : ?string {
        if ($show === true) {
            var_dump($this->sql);
            return null;
        }
        return $this->sql;
    }
}
