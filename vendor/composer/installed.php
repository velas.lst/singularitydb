<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => 'c638de8d41b8a436a5db94639c544990c536449e',
    'name' => 'evildev/singularitydb',
  ),
  'versions' => 
  array (
    'evildev/singularitydb' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => 'c638de8d41b8a436a5db94639c544990c536449e',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.59.1',
      'version' => '2.59.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a9000603ea337c8df16cc41f8b6be95a65f4d0f5',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9344f9cb97f3b19424af1a21a3b0e75b0a7d8d7e',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfa0ae98841b9e461207c13ab093d76b0fa7bace',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v6.0.9',
      'version' => '6.0.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ba011309943955a3807b8236c17cff3b88f67b6',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acbfbb274e730e5a0236f619b6168d9dedb3e282',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3|3.0',
      ),
    ),
  ),
);
